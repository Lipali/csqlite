package client;

import com.google.gson.Gson;

import java.io.*;
import java.net.Socket;

public class Client {
    public static void main(String[] args) throws IOException {
        Socket s = new Socket("localhost", 9999);
        DataOutputStream dout = new DataOutputStream(s.getOutputStream());
        DataInputStream din = new DataInputStream(s.getInputStream());
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        Gson gson = new Gson();
        String serverAnswer;
        String clientComend;
        String login;
        int messageNumbers;
        String messageNumbersString;

        do {
            System.out.println("PODAJ LOGIN :");
            login = br.readLine();
            String loginGson = gson.toJson(login);
            dout.writeUTF(loginGson);
            System.out.println("PODAJ HASŁO :");
            String key = br.readLine();
            String keyGson = gson.toJson(key);
            dout.writeUTF(keyGson);
            String serverAnswerGson = din.readUTF();
            serverAnswer = gson.fromJson(serverAnswerGson,String.class);

        }
        while(serverAnswer.equals("no"));

        String userRoleGson = din.readUTF();
        String userRole = gson.fromJson(userRoleGson,String.class);
        System.out.println("jesteś zalogowany jako : "+userRole);
        userRole.trim();

        if (userRole.equals("admin")){
            while(true) {
                System.out.println("Menu :");
                System.out.println("1 read");
                System.out.println("2 write");
                System.out.println("3 add");
                System.out.println("4 delete");
                System.out.println("5 readUser");
                System.out.println("6 exit");

                clientComend = br.readLine();
                dout.writeUTF(clientComend);

                if (clientComend.equals("exit")){
                    break;
                }
                else if (clientComend.equals("add")){
                    System.out.println("Podaj nowy login :");
                    String newLogin = br.readLine();
                    System.out.println("Podaj nowe hasło :");
                    String newkey = br.readLine();
                    System.out.println("Podaj nową funkcję");
                    String newFunction = br.readLine();
                    dout.writeUTF(newLogin);
                    dout.writeUTF(newkey);
                    dout.writeUTF(newFunction);
                }
                else if (clientComend.equals("delete")){
                    System.out.println("podaj login do usunięcia ");
                    String deletedLogin = br.readLine();
                    dout.writeUTF(deletedLogin);
                }
                else if (clientComend.equals("readUser")){
                    System.out.println("Czyje wiadomości chcesz odczytać ?");
                    String loginReaded = br.readLine();
                    dout.writeUTF(loginReaded);
                    messageNumbersString = din.readUTF();
                    messageNumbers =Integer.parseInt( messageNumbersString);
                    for (int i=0 ; i<messageNumbers;i++){
                        System.out.println(din.readUTF());
                    }
                }
                else if (clientComend.equals("read")){
                    dout.writeUTF(login);
                    messageNumbersString = din.readUTF();
                    messageNumbers =Integer.parseInt( messageNumbersString);
                    for (int i=0 ; i<messageNumbers;i++){
                        System.out.println(din.readUTF());
                    }
                }
                else if (clientComend.equals("write")){
                    System.out.println("Do kogo wysyłasz widomość ?");
                    String recipientMessage = br.readLine();
                    dout.writeUTF(recipientMessage);
                    messageNumbersString = din.readUTF();
                    messageNumbers =Integer.parseInt( messageNumbersString);
                    if (messageNumbers>4){
                        System.out.println("skrzynka przepełniona");
                    }
                    else{
                        System.out.println("podaj wiadomosć");
                        String message = br.readLine();
                        dout.writeUTF(message);
                    }
                }
            }
        }
        else{
            while(true) {
                System.out.println("Menu :");
                System.out.println("1 read");
                System.out.println("2 write");
                System.out.println("3 exit");

                clientComend = br.readLine();
                dout.writeUTF(clientComend);

                if (clientComend.equals("exit")){
                    break;
                }
                else if (clientComend.equals("read")){
                    dout.writeUTF(login);
                    messageNumbersString = din.readUTF();
                    messageNumbers =Integer.parseInt( messageNumbersString);
                    for (int i=0 ; i<messageNumbers;i++){
                        System.out.println(din.readUTF());
                    }
                }
                else if (clientComend.equals("write")){
                    System.out.println("Do kogo wysyłasz widomość ?");
                    String recipientMessage = br.readLine();
                    dout.writeUTF(recipientMessage);
                    messageNumbersString = din.readUTF();
                    messageNumbers =Integer.parseInt( messageNumbersString);
                    if (messageNumbers>4){
                        System.out.println("skrzynka przepełniona");
                    }
                    else{
                        System.out.println("podaj wiadomosć");
                        String message = br.readLine();
                        dout.writeUTF(message);
                    }
                }

            }
        }

    }

}
