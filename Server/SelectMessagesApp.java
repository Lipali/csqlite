package server;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SelectMessagesApp {
    private Connection connect() {
        String url = "jdbc:sqlite:C://sqlite/db/CS.db";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }

    public List<String> select(String recipient){
        List<String> list = new ArrayList<>();
        String temp;
        String sql = "SELECT sender, recipient, text"+" FROM messages WHERE recipient = ?";
        try (Connection conn = this.connect();
             PreparedStatement pstmt  = conn.prepareStatement(sql)){
             pstmt.setString(1,recipient);
             ResultSet rs    = pstmt.executeQuery();

            while (rs.next()) {
                temp =(rs.getString("sender") +  "\t" +
                       // rs.getString("recipient") + "\t" +
                        rs.getString("text"));
                list.add(temp);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return list;
    }
}
