package server;

import com.google.gson.Gson;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Server {

    public static void main(String[] args) throws IOException {
        //connect();
        //createNewDatabase("CS.db");
        //createUsersTable();
        //createMessagesTable();
        InsertUsersApp insertUsersApp = new InsertUsersApp();
        InsertMessagesApp insertMessagesApp = new InsertMessagesApp();
        //insertUsersApp.insert("Adam","1234","admin");
        //insertUsersApp.insert("Romek","12345","user");
        //insertUsersApp.insert("Ada","123","user");
        SelectUsersApp selectUsersApp = new SelectUsersApp();
        SelectMessagesApp selectMessagesApp = new SelectMessagesApp();
        ServerSocket ss = new ServerSocket(9999);
        Socket s = ss.accept();
        DataInputStream din = new DataInputStream(s.getInputStream());
        DataOutputStream dout = new DataOutputStream(s.getOutputStream());
        Gson gson = new Gson();
        String login;

        String userFunction;
        List <String> reader = new ArrayList<String>();
        String flagLog;
        String comand;
        do {
            String loginGson = din.readUTF();
            login = gson.fromJson(loginGson, String.class);
            String passGson = din.readUTF();
            String pass = gson.fromJson(passGson, String.class);
            String passFromDataBase = selectUsersApp.selectUserKey(login).trim();
            userFunction = selectUsersApp.selectUserFunction(login);

            if (passFromDataBase.equals(pass)){
                flagLog = "yes";}
            else {
                flagLog = "no";}

            String serverAnswerGson = gson.toJson(flagLog);
            dout.writeUTF(serverAnswerGson);
        }
        while(flagLog.equals("no"));


        dout.writeUTF(userFunction);
        String userFunctionTemp =userFunction.trim();

        if (userFunctionTemp.equals("admin")){
            while(true) {
                comand = din.readUTF();

                if (comand.equals("exit")){
                    break;
                }
                else if (comand.equals("add")){
                    String newLogin = din.readUTF();
                    String newKey = din.readUTF();
                    String newFunction = din.readUTF();
                    insertUsersApp.insert(newLogin,newKey,newFunction);
                }
                else if (comand.equals("delete")){
                    String deleteLogin = din.readUTF();
                    insertUsersApp.delete(deleteLogin);
                }
                else if (comand.equals("readUser")){
                    String readUser =din.readUTF();
                    reader =selectMessagesApp.select(readUser);
                    dout.writeUTF(String.valueOf(reader.size()));
                    for (String x : reader){
                        dout.writeUTF(x);
                    }
                }
                else if (comand.equals("read")){
                    reader =selectMessagesApp.select(login);
                    dout.writeUTF(String.valueOf(reader.size()));
                    for (String x : reader){
                        dout.writeUTF(x);
                    }
                }
                else if (comand.equals("write")){
                    String recipientLogin= din.readUTF();
                    reader = selectMessagesApp.select(recipientLogin);
                    int messageNumber = reader.size();
                    dout.writeUTF(String.valueOf(messageNumber));
                    if (messageNumber>4){
                    }
                    else{
                        String text = din.readUTF();
                        insertMessagesApp.insert(login,recipientLogin,text);
                    }
                }


            }
        }
        else {
            while(true) {
                comand = din.readUTF();

                if (comand.equals("exit")){
                    break;
                }
                else if (comand.equals("read")){
                    reader =selectMessagesApp.select(login);
                    dout.writeUTF(String.valueOf(reader.size()));
                    for (String x : reader){
                        dout.writeUTF(x);
                    }

                }
                else if (comand.equals("write")){
                    String recipientLogin= din.readUTF();
                    reader = selectMessagesApp.select(recipientLogin);
                    int messageNumber = reader.size();
                    dout.writeUTF(String.valueOf(messageNumber));
                    if (messageNumber>4){
                    }
                    else{
                        String text = din.readUTF();
                        insertMessagesApp.insert(login,recipientLogin,text);
                    }
                }

            }
        }

    }



    public static void lcreateUsersTable() {
        String url = "jdbc:sqlite:C://sqlite/db/CS.db";
        String sql = "CREATE TABLE IF NOT EXISTS  users(" + "n\n"
                + "	id integer PRIMARY KEY,\n"
                + "	name text ,\n"
                + "	key text ,\n"
                + "	function text \n"
                + ");";
        try (Connection conn = DriverManager.getConnection(url);
             Statement stmt = conn.createStatement()) {
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    public static void createMessagesTable() {
        String url = "jdbc:sqlite:C://sqlite/db/CS.db";
        String sql = "CREATE TABLE IF NOT EXISTS messages ( " + "\n"
                + "	id integer PRIMARY KEY,\n"
                + "	sender text ,\n"
                + "	recipient text ,\n"
                + "	text text \n"
                + ");";
        try (Connection conn = DriverManager.getConnection(url);
             Statement stmt = conn.createStatement()) {
            stmt.execute(sql);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    public static void connect() {
        Connection conn = null;
        try {
            String url = "jdbc:sqlite:C:/sqlite/db/chinook.db";
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been established.");

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
    public static void createNewDatabase(String fileName) {

        String url = "jdbc:sqlite:C:/sqlite/db/" + fileName;

        try (Connection conn = DriverManager.getConnection(url)) {
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                System.out.println("A new database has been created.");
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

}
