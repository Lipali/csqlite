package server;

import java.sql.*;

public class SelectUsersApp {
    private Connection connect() {
        String url = "jdbc:sqlite:C://sqlite/db/CS.db";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }

    public String selectUserKey(String name){
        String goal = "";
        String sql = "SELECT key  "
                + "FROM users WHERE name = ? ";

        try (Connection conn = this.connect();
             PreparedStatement pstmt  = conn.prepareStatement(sql)){

            pstmt.setString(1,name);
            ResultSet rs  = pstmt.executeQuery();

            while (rs.next()) {

                    goal = rs.getString("key") + "\t";

            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return goal;
    }
    public String selectUserFunction(String name){
        String goal = "";
        String sql = "SELECT function  "
                + "FROM users WHERE name = ? ";

        try (Connection conn = this.connect();
             PreparedStatement pstmt  = conn.prepareStatement(sql)){

            pstmt.setString(1,name);
            ResultSet rs  = pstmt.executeQuery();

            while (rs.next()) {

                goal = rs.getString("function") + "\t";

            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return goal;
    }


}
